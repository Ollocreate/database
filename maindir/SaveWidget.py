from PyQt5 import QtCore, QtGui, QtWidgets
import os
from main import data
from main import sort
from main import write_sort

class Ui_widg_Save(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

    def setupUi(self, widg_Save):
        widg_Save.setObjectName("widg_Save")
        widg_Save.resize(253, 102)
        widg_Save.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.528364, y1:1, x2:0.539773, y2:0.158636, stop:0.164773 rgba(12, 120, 77, 255), stop:0.676136 rgba(255, 255, 255, 255), stop:0.880682 rgba(255, 255, 255, 255));")
        self.btn_SaveSumbit = QtWidgets.QPushButton(widg_Save)
        self.btn_SaveSumbit.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btn_SaveSumbit.setGeometry(QtCore.QRect(100, 60, 64, 22))
        self.btn_SaveSumbit.setStyleSheet("background-color:  rgb(202, 135, 0);\n"
"color: #fff;\n"
"font-size: 10px;\n"
"border-radius: 6px;\n"
"font: 8pt \"Verdana\";")
        self.btn_SaveSumbit.setObjectName("btn_SaveSumbit")
        self.btn_SaveSumbit.clicked.connect(self.saving)

        self.widget = QtWidgets.QWidget(widg_Save)
        self.widget.setGeometry(QtCore.QRect(40, 30, 179, 20))
        self.widget.setObjectName("widget")
        self.la_Save = QtWidgets.QHBoxLayout(self.widget)
        self.la_Save.setContentsMargins(0, 0, 0, 0)
        self.la_Save.setObjectName("la_Save")
        self.lbl_Save = QtWidgets.QLabel(self.widget)
        self.lbl_Save.setStyleSheet("font: 8pt \"Verdana\";\n"
"background-color: none;")
        self.lbl_Save.setObjectName("lbl_Save")
        self.la_Save.addWidget(self.lbl_Save)
        self.inp_Save = QtWidgets.QLineEdit(self.widget)
        self.inp_Save.setStyleSheet("font: 9pt \"Verdana\";\n"
"border-radius: 5px;\n"
"border: 1px solid rgb(144, 144, 144);\n"
"background-color: #fff;")
        self.inp_Save.setObjectName("inp_Save")
        self.la_Save.addWidget(self.inp_Save)

        self.retranslateUi(widg_Save)
        QtCore.QMetaObject.connectSlotsByName(widg_Save)

    def retranslateUi(self, widg_Save):
        _translate = QtCore.QCoreApplication.translate
        widg_Save.setWindowTitle(_translate("widg_Save", "Сохранить как"))
        self.btn_SaveSumbit.setText(_translate("widg_Save", "Сохранить"))
        self.lbl_Save.setText(_translate("widg_Save", "Введите имя файла"))

    def saving(self): #сортировка и переименование файла
        file_name = self.inp_Save.text()
        write_sort(sort(data()))
        os.rename("database.txt", "%s.txt" % file_name)
        Ui_widg_Save.close(self)
