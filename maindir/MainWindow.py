from PyQt5 import QtCore, QtGui, QtWidgets
import NoteForm
import SaveWidget
import SearchWidget

class Ui_MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(520, 200)
        MainWindow.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.528364, y1:1, x2:0.539773, y2:0.158636, stop:0.164773 rgba(12, 120, 77, 255), stop:0.676136 rgba(255, 255, 255, 255), stop:0.880682 rgba(255, 255, 255, 255));")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.lbl_Title = QtWidgets.QLabel(self.centralwidget)
        self.lbl_Title.setGeometry(QtCore.QRect(210, 30, 150, 45))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(30)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        self.lbl_Title.setFont(font)
        self.lbl_Title.setStyleSheet("font: 30pt \"Times New Roman\";\n"
"background-color: #fff;")
        self.lbl_Title.setObjectName("lbl_Title")
        self.btn_Record = QtWidgets.QPushButton(self.centralwidget)
        self.btn_Record.setEnabled(True)
        self.btn_Record.setGeometry(QtCore.QRect(20, 100, 156, 40))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_Record.sizePolicy().hasHeightForWidth())
        self.btn_Record.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Verdana")
        font.setPointSize(10)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        font.setKerning(True)
        self.btn_Record.setFont(font)
        self.btn_Record.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btn_Record.setMouseTracking(False)
        self.btn_Record.setTabletTracking(False)
        self.btn_Record.setAcceptDrops(False)
        self.btn_Record.setStyleSheet("background-color:  rgb(202, 135, 0);\n"
"color: #fff;\n"
"font-size: 12px;\n"
"border-radius: 10px;\n"
"font: 10pt \"Verdana\";")
        self.btn_Record.setObjectName("btn_Record")
        self.btn_Record.clicked.connect(self.form_window)

        self.btn_Search = QtWidgets.QPushButton(self.centralwidget)
        self.btn_Search.setGeometry(QtCore.QRect(201, 100, 83, 40))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_Search.sizePolicy().hasHeightForWidth())
        self.btn_Search.setSizePolicy(sizePolicy)
        self.btn_Search.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btn_Search.setMouseTracking(False)
        self.btn_Search.setTabletTracking(False)
        self.btn_Search.setAcceptDrops(False)
        self.btn_Search.setStyleSheet("background-color:  rgb(202, 135, 0);\n"
"color: #fff;\n"
"font-size: 12px;\n"
"border-radius: 10px;\n"
"font: 10pt \"Verdana\";")
        self.btn_Search.setObjectName("btn_Search")
        self.btn_Search.clicked.connect(self.search_window)

        self.btn_Save = QtWidgets.QPushButton(self.centralwidget)
        self.btn_Save.setGeometry(QtCore.QRect(310, 100, 190, 40))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_Save.sizePolicy().hasHeightForWidth())
        self.btn_Save.setSizePolicy(sizePolicy)
        self.btn_Save.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btn_Save.setMouseTracking(False)
        self.btn_Save.setTabletTracking(False)
        self.btn_Save.setAcceptDrops(False)
        self.btn_Save.setStyleSheet("background-color:  rgb(202, 135, 0);\n"
"color: #fff;\n"
"font-size: 12px;\n"
"border-radius: 10px;\n"
"font: 10pt \"Verdana\";")
        self.btn_Save.setObjectName("btn_Save")
        self.btn_Save.clicked.connect(self.save_window)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "DeathNote"))
        self.lbl_Title.setText(_translate("MainWindow", " Note"))
        self.btn_Record.setText(_translate("MainWindow", "Начать запись"))
        self.btn_Search.setText(_translate("MainWindow", "Найти"))
        self.btn_Save.setText(_translate("MainWindow", "Сохранить в файл"))

    def form_window(self): #открытие окна записи
        self.w = NoteForm.Ui_win_Form()
        self.w.show()

    def search_window(self): #открытие окна поиска
        self.w = SearchWidget.Ui_widg_Search()
        self.w.show()

    def save_window(self): #открытие окна сохранения
        self.w = SaveWidget.Ui_widg_Save()
        self.w.show()