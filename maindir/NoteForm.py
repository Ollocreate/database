from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_win_Form(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

    def file(self):
        database = open('database.txt', 'w')
        database.close()

    def setupUi(self, win_Form):
        win_Form.setObjectName("win_Form")
        win_Form.resize(349, 205)
        win_Form.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.528364, y1:1, x2:0.539773, y2:0.158636, stop:0.164773 rgba(12, 120, 77, 255), stop:0.676136 rgba(255, 255, 255, 255), stop:0.880682 rgba(255, 255, 255, 255));")
        self.centralwidget = QtWidgets.QWidget(win_Form)
        self.centralwidget.setObjectName("centralwidget")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(30, 20, 301, 131))
        self.widget.setObjectName("widget")
        self.lay_Form = QtWidgets.QVBoxLayout(self.widget)
        self.lay_Form.setContentsMargins(0, 0, 0, 0)
        self.lay_Form.setObjectName("lay_Form")
        self.lay_Name = QtWidgets.QHBoxLayout()
        self.lay_Name.setObjectName("lay_Name")
        self.lbl_Name = QtWidgets.QLabel(self.widget)
        self.lbl_Name.setStyleSheet("font: 8pt \"Verdana\";\n"
    "background-color: none;")
        self.lbl_Name.setObjectName("lbl_Name")
        self.lay_Name.addWidget(self.lbl_Name)
        self.inp_Name = QtWidgets.QLineEdit(self.widget)
        self.inp_Name.setStyleSheet("font: 9pt \"Verdana\";\n"
    "border-radius: 5px;\n"
    "border: 1px solid rgb(144, 144, 144);\n"
    "background-color: #fff;\n"
    "")
        self.inp_Name.setObjectName("inp_Name")
        self.lay_Name.addWidget(self.inp_Name)
        

        self.lay_Form.addLayout(self.lay_Name)
        self.lay_Surname = QtWidgets.QHBoxLayout()
        self.lay_Surname.setObjectName("lay_Surname")
        self.lbl_Surname = QtWidgets.QLabel(self.widget)
        self.lbl_Surname.setStyleSheet("font: 8pt \"Verdana\";\n"
    "background-color: none;")
        self.lbl_Surname.setObjectName("lbl_Surname")
        self.lay_Surname.addWidget(self.lbl_Surname)
        self.inp_Surname = QtWidgets.QLineEdit(self.widget)
        self.inp_Surname.setStyleSheet("font: 9pt \"Verdana\";\n"
    "border-radius: 5px;\n"
    "border: 1px solid rgb(144, 144, 144);\n"
    "background-color: #fff;")
        self.inp_Surname.setObjectName("inp_Surname")
        

        self.lay_Surname.addWidget(self.inp_Surname)
        self.lay_Form.addLayout(self.lay_Surname)
        self.lay_Phone = QtWidgets.QHBoxLayout()
        self.lay_Phone.setObjectName("lay_Phone")
        self.lbl_Phone = QtWidgets.QLabel(self.widget)
        self.lbl_Phone.setStyleSheet("font: 8pt \"Verdana\";\n"
    "background-color: none;")
        self.lbl_Phone.setObjectName("lbl_Phone")
        self.lay_Phone.addWidget(self.lbl_Phone)
        self.inp_Phone = QtWidgets.QLineEdit(self.widget)
        self.inp_Phone.setStyleSheet("font: 9pt \"Verdana\";\n"
    "border-radius: 5px;\n"
    "border: 1px solid rgb(144, 144, 144);\n"
    "background-color: #fff;")
        self.inp_Phone.setObjectName("inp_Phone")


        self.lay_Phone.addWidget(self.inp_Phone)
        self.lay_Form.addLayout(self.lay_Phone)
        self.lay_Birth = QtWidgets.QHBoxLayout()
        self.lay_Birth.setObjectName("lay_Birth")
        self.lbl_Birth = QtWidgets.QLabel(self.widget)
        self.lbl_Birth.setStyleSheet("font: 8pt \"Verdana\";\n"
    "background-color: none;")
        self.lbl_Birth.setObjectName("lbl_Birth")
        self.lay_Birth.addWidget(self.lbl_Birth)
        self.inp_Birth = QtWidgets.QLineEdit(self.widget)
        self.inp_Birth.setStyleSheet("font: 9pt \"Verdana\";\n"
    "border-radius: 5px;\n"
    "border: 1px solid rgb(144, 144, 144);\n"
    "background-color: #fff;")
        self.inp_Birth.setText("")
        self.inp_Birth.setObjectName("inp_Birth")
        

        self.lay_Birth.addWidget(self.inp_Birth)
        self.lay_Form.addLayout(self.lay_Birth)
        self.widget1 = QtWidgets.QWidget(self.centralwidget)
        self.widget1.setGeometry(QtCore.QRect(90, 150, 181, 21))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.widget1.sizePolicy().hasHeightForWidth())
        self.widget1.setSizePolicy(sizePolicy)
        self.widget1.setObjectName("widget1")
        self.lay_RecBtn = QtWidgets.QHBoxLayout(self.widget1)
        self.lay_RecBtn.setContentsMargins(0, 0, 0, 0)
        self.lay_RecBtn.setObjectName("lay_RecBtn")
        self.btn_RecCont = QtWidgets.QPushButton(self.widget1)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_RecCont.sizePolicy().hasHeightForWidth())
        self.btn_RecCont.setSizePolicy(sizePolicy)
        self.btn_RecCont.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btn_RecCont.setStyleSheet("background-color:  rgb(202, 135, 0);\n"
    "color: #fff;\n"
    "font-size: 10px;\n"
    "border-radius: 6px;\n"
    "font: 8pt \"Verdana\";")
        self.btn_RecCont.setObjectName("btn_RecCont")
        self.btn_RecCont.clicked.connect(self.recording)  

        self.lay_RecBtn.addWidget(self.btn_RecCont)
        self.btn_RecEnd = QtWidgets.QPushButton(self.widget1)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btn_RecEnd.sizePolicy().hasHeightForWidth())
        self.btn_RecEnd.setSizePolicy(sizePolicy)
        self.btn_RecEnd.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btn_RecEnd.setStyleSheet("background-color:  rgb(202, 135, 0);\n"
    "color: #fff;\n"
    "font-size: 10px;\n"
    "border-radius: 6px;\n"
    "font: 8pt \"Verdana\";")
        self.btn_RecEnd.setObjectName("btn_RecEnd")
        self.btn_RecEnd.clicked.connect(self.recording_end)  

        self.lay_RecBtn.addWidget(self.btn_RecEnd)
        win_Form.setCentralWidget(self.centralwidget)

        self.retranslateUi(win_Form)
        QtCore.QMetaObject.connectSlotsByName(win_Form)
        
    def retranslateUi(self, win_Form):
        _translate = QtCore.QCoreApplication.translate
        win_Form.setWindowTitle(_translate("win_Form", "Запись"))
        self.lbl_Name.setText(_translate("win_Form", "Имя"))
        self.inp_Name.setPlaceholderText(_translate("win_Form", "Иванов"))
        self.lbl_Surname.setText(_translate("win_Form", "Фамилия"))
        self.inp_Surname.setPlaceholderText(_translate("win_Form", "Иван"))
        self.lbl_Phone.setText(_translate("win_Form", "Номер телефона"))
        self.inp_Phone.setPlaceholderText(_translate("win_Form", "89233456556"))
        self.lbl_Birth.setText(_translate("win_Form", "Дата рождения"))
        self.inp_Birth.setPlaceholderText(_translate("win_Form", "12.12.2000"))
        self.btn_RecCont.setText(_translate("win_Form", "Записать ещё"))
        self.btn_RecEnd.setText(_translate("win_Form", "Закончить запись"))

    def recording(self):  #запись в файл(продолжение)
        database = open('database.txt', 'a')

        Name = self.inp_Name.text()
        Surname = self.inp_Surname.text() 
        Phone = self.inp_Phone.text() 
        Birth = self.inp_Birth.text() 

        database.write("{0},{1},{2},{3}\n".format(Name, Surname, Phone, Birth))
        database.close()

        self.inp_Name.setText("")
        self.inp_Surname.setText("")
        self.inp_Phone.setText("")
        self.inp_Birth.setText("")

    def recording_end(self): #запись в файл(окончание)
        database = open('database.txt', 'a')

        Name = self.inp_Name.text()
        Surname = self.inp_Surname.text() 
        Phone = self.inp_Phone.text() 
        Birth = self.inp_Birth.text() 

        self.inp_Name.setText("")
        self.inp_Surname.setText("")
        self.inp_Phone.setText("")
        self.inp_Birth.setText("")
        
        database.write("{0},{1},{2},{3}\n".format(Name, Surname, Phone, Birth))
        database.close()
        Ui_win_Form.close(self)