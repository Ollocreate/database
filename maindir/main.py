from PyQt5 import *
import MainWindow
import sys
import datetime
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *


def data(): #чтение массива
    file = open("database.txt", "rt")
    data_massive = []
    for line in file:
        if line == "\n":
            continue
        line = line.strip()
        line = line.split(",")
        data_massive.append(line)
    return data_massive

def sort(data_massive): #сортировка
    for i in range(len(data_massive)):
        age = data_massive[i][3]
        age = age.split(".")
        print(age)
        for j in range(len(age)):
            age[j] = int(age[j])
        date = datetime.date(age[2], age[1], age[0])
        data_massive[i][3] = date
    sorted_mas = sorted(data_massive, key=lambda i: i[3])
    for i in range(len(sorted_mas)):
        sorted_mas[i][3] = sorted_mas[i][3].strftime("%d %m %Y")
        age = sorted_mas[i][3].split(" ")
        sorted_mas[i][3] = age
    return sorted_mas

def write_sort(sorted_mas): #запись сортированного массива
    database = open("database.txt", "wt")
    for i in range(len(sorted_mas)):
        database.write("{0},{1},{2},".format(sorted_mas[i][0], sorted_mas[i][1], sorted_mas[i][2]))
        database.write("{0}.{1}.{2}\n".format(sorted_mas[i][3][0], sorted_mas[i][3][1], sorted_mas[i][3][2]))
    database.close()

def application(): #запуск приложения
    app = QApplication(sys.argv)
    win = MainWindow.Ui_MainWindow()
    win.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    application()
    
