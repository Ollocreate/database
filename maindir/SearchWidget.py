from PyQt5 import QtCore, QtGui, QtWidgets
from main import data

class Ui_widg_Search(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

    def setupUi(self, widg_Search):
        widg_Search.setObjectName("widg_Search")
        widg_Search.resize(303, 153)
        widg_Search.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.528364, y1:1, x2:0.539773, y2:0.158636, stop:0.164773 rgba(12, 120, 77, 255), stop:0.676136 rgba(255, 255, 255, 255), stop:0.880682 rgba(255, 255, 255, 255));")
        self.btn_SearchSubmit = QtWidgets.QPushButton(widg_Search)
        self.btn_SearchSubmit.setGeometry(QtCore.QRect(110, 100, 90, 25))
        self.btn_SearchSubmit.setCursor(QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.btn_SearchSubmit.setStyleSheet("background-color:  rgb(202, 135, 0);\n"
"color: #fff;\n"
"font-size: 10px;\n"
"border-radius: 6px;\n"
"font: 8pt \"Verdana\";")
        self.btn_SearchSubmit.setObjectName("btn_SearchSubmit")
        self.btn_SearchSubmit.clicked.connect(self.searching)  


        self.layoutWidget = QtWidgets.QWidget(widg_Search)
        self.layoutWidget.setGeometry(QtCore.QRect(50, 70, 197, 21))
        self.layoutWidget.setObjectName("layoutWidget")
        self.lay_SearchForm = QtWidgets.QHBoxLayout(self.layoutWidget)
        self.lay_SearchForm.setContentsMargins(0, 0, 0, 0)
        self.lay_SearchForm.setObjectName("lay_SearchForm")
        self.lbl_Search = QtWidgets.QLabel(self.layoutWidget)
        self.lbl_Search.setStyleSheet("font: 8pt \"Verdana\";\n"
"background-color: none;")
        self.lbl_Search.setObjectName("lbl_Search")
        self.lay_SearchForm.addWidget(self.lbl_Search)
        self.inp_Search = QtWidgets.QLineEdit(self.layoutWidget)
        self.inp_Search.setStyleSheet("font: 9pt \"Verdana\";\n"
"border-radius: 5px;\n"
"border: 1px solid rgb(144, 144, 144);\n"
"background-color: #fff;")
        self.inp_Search.setObjectName("inp_Search")
        self.lay_SearchForm.addWidget(self.inp_Search)
        self.label = QtWidgets.QLabel(widg_Search)
        self.label.setGeometry(QtCore.QRect(40, 20, 220, 31))
        self.label.setStyleSheet("background-color: none")
        self.label.setObjectName("label")
        self.label.hide()

        self.retranslateUi(widg_Search)
        QtCore.QMetaObject.connectSlotsByName(widg_Search)

    def retranslateUi(self, widg_Search):
        _translate = QtCore.QCoreApplication.translate
        widg_Search.setWindowTitle(_translate("widg_Search", "Поиск"))
        self.btn_SearchSubmit.setText(_translate("widg_Search", "Начать поиск"))
        self.lbl_Search.setText(_translate("widg_Search", "Телефона"))
        self.inp_Search.setPlaceholderText(_translate("widg_Search", "89122345665"))

    
    def searching(self):  #поиск 
        data_s = data()  
        Phone = self.inp_Search.text()
        for tel in range(len(data_s)):
            if data_s[tel][2] == Phone:
                self.label.setText("Найдено: {0} {1}, {2}".format(data_s[tel][0], data_s[tel][1], data_s[tel][3]))
                self.label.show()
                break
            else:
                self.label.setText("Такого человека нет в базе")
                self.label.show()
