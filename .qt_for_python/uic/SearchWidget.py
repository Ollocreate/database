# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'c:\Users\Home\Desktop\labs\Python\laba7\ver. 1\ui\SearchWidget.ui'
#
# Created by: PyQt5 UI code generator 5.15.6
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_widg_Search(object):
    def setupUi(self, widg_Search):
        widg_Search.setObjectName("widg_Search")
        widg_Search.resize(303, 124)
        widg_Search.setStyleSheet("background-color: qlineargradient(spread:pad, x1:0.528364, y1:1, x2:0.539773, y2:0.158636, stop:0.164773 rgba(12, 120, 77, 255), stop:0.676136 rgba(255, 255, 255, 255), stop:0.880682 rgba(255, 255, 255, 255));")
        self.btn_SearchSubmit = QtWidgets.QPushButton(widg_Search)
        self.btn_SearchSubmit.setGeometry(QtCore.QRect(110, 70, 71, 17))
        self.btn_SearchSubmit.setStyleSheet("background-color:  rgb(202, 135, 0);\n"
"color: #fff;\n"
"font-size: 10px;\n"
"border-radius: 6px;\n"
"font: 8pt \"Verdana\";")
        self.btn_SearchSubmit.setObjectName("btn_SearchSubmit")
        self.widget = QtWidgets.QWidget(widg_Search)
        self.widget.setGeometry(QtCore.QRect(50, 40, 197, 21))
        self.widget.setObjectName("widget")
        self.lay_SearchForm = QtWidgets.QHBoxLayout(self.widget)
        self.lay_SearchForm.setContentsMargins(0, 0, 0, 0)
        self.lay_SearchForm.setObjectName("lay_SearchForm")
        self.lbl_Search = QtWidgets.QLabel(self.widget)
        self.lbl_Search.setStyleSheet("font: 8pt \"Verdana\";\n"
"background-color: none;")
        self.lbl_Search.setObjectName("lbl_Search")
        self.lay_SearchForm.addWidget(self.lbl_Search)
        self.inp_Search = QtWidgets.QLineEdit(self.widget)
        self.inp_Search.setStyleSheet("font: 9pt \"Verdana\";\n"
"border-radius: 5px;\n"
"border: 1px solid rgb(144, 144, 144);\n"
"background-color: #fff;")
        self.inp_Search.setObjectName("inp_Search")
        self.lay_SearchForm.addWidget(self.inp_Search)

        self.retranslateUi(widg_Search)
        QtCore.QMetaObject.connectSlotsByName(widg_Search)

    def retranslateUi(self, widg_Search):
        _translate = QtCore.QCoreApplication.translate
        widg_Search.setWindowTitle(_translate("widg_Search", "Поиск"))
        self.btn_SearchSubmit.setText(_translate("widg_Search", "Начать поиск"))
        self.lbl_Search.setText(_translate("widg_Search", "Введите номер телефона"))
        self.inp_Search.setPlaceholderText(_translate("widg_Search", "89122345665"))
